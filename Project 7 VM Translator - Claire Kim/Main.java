import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Main
 * Calls for construction of Parser to parse the VM input file, that will then translate file to assembly code.
 */

public class Main {

    public static void main(String[] args) throws Exception {

            // Parse command line
            if (args.length == 0) {
                System.out.println("Please specify a directory or .vm file.");
                System.exit(1);
            }

            else if (args.length > 0) {

                File file = new File(args[0]);
                boolean isDirectory = file.isDirectory();

                // If single file
                String singleFileName = args[0];

                if (!isDirectory) {
                    if (singleFileName.endsWith(".vm")) {
                        try {

                            // Creates and writes out to asm file with same name as vm
                            String asmFileName = args[0].replace(".vm", ".asm");
                            FileWriter fw = new FileWriter(asmFileName);

                            // Assign name to String ArrayList of translated commands returned by the getParser method
                            ArrayList<String> asm = getParser(singleFileName);

                            // Then iterate through this asm that collected all of the translated commands
                            for (int i = 0; i < asm.size(); i++) {
                                String currCommand = asm.get(i);
                                fw.write(currCommand + "\n");

                                // Close filewriter when done
                                if (i == asm.size() - 1) {
                                    fw.close();
                                }
                            }

                        } catch (IOException e) {
                            System.out.println("Please specify a directory or .vm file.");
                        }
                    } else {
                        System.out.println("File must be an .vm file.");
                    }
                }

                // If directory of multiple files
                else if (isDirectory) {
                    File[] files = file.listFiles();

                    for (File f : files) {
                        if (f.toString().endsWith(".vm")) {

                            String vmFileName = f.toString();

                            // Creates and writes out to asm files with same name as vm
                            String asmFileName = vmFileName.replace(".vm", ".asm");
                            FileWriter fw = new FileWriter(asmFileName);

                            try {
                                // As above, simply writes line by line after all the work is done
                                ArrayList<String> asm = getParser(vmFileName);

                                // Then iterate through this asm that collected all of the translated commands
                                for (int i = 0; i < asm.size(); i++) {
                                    String currCommand = asm.get(i);
                                    fw.write(currCommand + "\n");

                                    // Close filewriter when done
                                    if (i == asm.size() - 1) {
                                        fw.close();
                                    }
                                }
                            }
                            catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
    }

    // Instantiate a Parser and return an ArrayList of the commands in the file passed into it, now translated.
    private static ArrayList<String> getParser(String inFileName) throws Exception {

        // Instantiate ArrayList to be filled with translated commands
        ArrayList<String> asmCommands = new ArrayList<String>();

        // Instantiate Parser to be opened for this file
        Parser parser = new Parser(inFileName);

        // Assign name to String ArrayList
        ArrayList<String> file = parser.getVmCommands();  // gets all the untranslated commands

        // March through and parse line by line
        for (int j = 0; j < parser.getVmCommands().size(); j++) {

            String currentCommand = file.get(j);
            String rootName = inFileName.substring(0, inFileName.indexOf('.'));
            // Add to the ArrayList of translated commands
            asmCommands.add(parser.parse(currentCommand, rootName));
        }
        return asmCommands;
    }

}
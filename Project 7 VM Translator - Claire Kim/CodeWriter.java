/**
 * CodeWriter
 * Stipulates the translation of the commands
 */

public class CodeWriter {

    // Set a static int to be incremented with each use to distinguish command label symbols. (StackTest will not
    // complete successfully without this)
    public static int label = 0;

    // Translate vm push commands to assembly commands
    public String writePush (String segment, int index, String vmName) {

        String asmCommandString = null;

        // Push command sequence in common for ARG, LCL, THIS, THAT
        String pushArgLclThisThatBody =
                          "D=M\n"
                        + "@" + index + "\n"
                        + "A=D+A\n"
                        + "D=M\n"
                        + "@SP\n"
                        + "AM=M+1\n"
                        + "A=A-1\n"
                        + "M=D";

        // Push command sequence in common for static, constant, pointer, temp
        String pushStatConstPtTempBody =
                         "@SP\n"
                        + "AM=M+1\n"
                        + "A=A-1\n"
                        + "M=D";

        switch(segment){

            case "argument":
                asmCommandString = "@ARG\n" + pushArgLclThisThatBody;
                break;

            case "local":
                asmCommandString = "@LCL\n" + pushArgLclThisThatBody;
                break;

            case "this":
                asmCommandString = "@THIS\n" + pushArgLclThisThatBody;
                break;

            case "that":
                asmCommandString = "@THAT\n" + pushArgLclThisThatBody;
                break;

            case "static":
                asmCommandString = "@" + (vmName + "." + index )+ "\n"
                        + "D=M\n"
                        + pushStatConstPtTempBody;
                break;

            case "constant":
                asmCommandString = "@" + index + "\n"
                        + "D=A\n"
                        + pushStatConstPtTempBody;
                break;

            case "pointer":
                asmCommandString = "@R" + (3+index) + "\n"
                        + "D=M\n"
                        + pushStatConstPtTempBody;
                break;
            case "temp":
                asmCommandString = "@R" + (5+index) + "\n"
                        + "D=M\n"
                        + pushStatConstPtTempBody;
                break;

        }
        return asmCommandString;

    }

    // Translate vm pop commands to assembly commands (pop from top of stack, store in memory)
    public String writePop (String segment, int index, String vmName) {
        String asmCommandString = null;

        // Pop command sequence in common for ARG, LCL, THIS, THAT
        String popArgLclThisThatBody = "D=M\n"
                + "@" + index + "\n"
                + "D=D+A\n"
                + "@R13\n"
                + "M=D\n"
                + "@SP\n"
                + "AM=M-1\n"
                + "D=M\n"
                + "@R13\n"
                + "A=M\n"
                + "M=D";

        // Push command sequence in common for static, pointer, temp
        String popStatPtTempBody = "@SP\n"
                + "AM=M-1\n"
                + "D=M\n";


        switch (segment) {
            case "argument":
                asmCommandString = "@ARG\n" + popArgLclThisThatBody;
                break;

            case "local":
                asmCommandString = "@LCL\n" + popArgLclThisThatBody;
                break;

            case "this":
                asmCommandString = "@THIS\n" + popArgLclThisThatBody;
                break;
            case "that":
                asmCommandString = "@THAT\n" + popArgLclThisThatBody;
                break;


            case "pointer":
                asmCommandString = popStatPtTempBody
                        + "@R" + (3+index) + "\n"
                        + "M=D";
                break;

            case "static":
                asmCommandString = popStatPtTempBody
                        + "@" + (vmName + "." + index) + "\n"
                        + "M=D";
                break;

            case "temp":
                asmCommandString = popStatPtTempBody
                        + "@R" + (5+index) + "\n"
                        + "M=D";
                break;
        }
        return asmCommandString;
    }


    // Translate to arithmetic and logical assembly commands
    public String writeArithmetic(String command) {
        String asmCommandString = null;

        // Template of command sequence strings in common with groups of commands
        String addSubBody =
                  "@SP\n"
                + "AM=M-1\n"
                + "D=M\n"
                + "A=A-1\n";

        String negNotBody = "@SP\n"
                + "A=M-1\n";

        String andOrBody =
                  "@SP\n"
                + "AM=M-1\n"
                + "D=M\n"
                + "A=A-1\n";

        String eqLtGtBodyStart =
                  "@SP\n"
                + "AM=M-1\n"
                + "D=M\n"
                + "A=A-1\n";

        String eqLtGtBodyEnd =
                  "@SP\n"
                + "A=M-1\n"
                + "M=-1\n";


        switch (command) {

            case "add":
                asmCommandString = addSubBody
                        + "M=D+M";
                break;

            case "sub":
                asmCommandString = addSubBody
                        + "M=M-D";
                break;

            case "neg":
                asmCommandString = negNotBody
                        + "M=-M";
                break;

            case "not":
                asmCommandString = negNotBody
                        + "M=!M";
                break;


            case "eq":
                asmCommandString = eqLtGtBodyStart
                        + "D=M-D\n"
                        + "M=0\n"
                        + "@END." + label + "\n"
                        + "D;JNE\n"
                        + eqLtGtBodyEnd + "(END." + label +")";
                        label++;
                break;

            case "gt":
                asmCommandString = eqLtGtBodyStart
                        + "D=D-M\n"
                        + "M=0\n"
                        + "@END." + label + "\n"
                        + "D;JGE\n"
                        + eqLtGtBodyEnd + "(END." + label + ")";
                        label++;
                break;

            case "lt":
                asmCommandString = eqLtGtBodyStart
                        + "D=D-M\n"
                        + "M=0\n"
                        + "@END." + label + "\n"
                        + "D;JLE\n"
                        + eqLtGtBodyEnd + "(END." + label + ")";
                        label++;
                break;


            case "and":
                asmCommandString = andOrBody
                        + "M=D&M";
                break;

            case "or":
                asmCommandString = andOrBody
                        + "M=D|M";
                break;
        }

        return asmCommandString;
    }
}
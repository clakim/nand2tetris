import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.lang.Integer;

/**
 * Parser
 * Handles the parsing of a .vm file and accesses the input code, removing whitespace
 * Constructs a CodeWriter to generate code into the corresponding output file
 */

public class Parser {

    private String currCommand;

    // Instantiate ArrayList for vm commands stripped to essence
    ArrayList<String> vmCommands = new ArrayList<>();

    // Parser constructor calls the white-space-removing method that opens the vm input filestream and stores vm commands to an ArrayList
    public Parser(String inputFileName) throws Exception {
        {
            // Call removeWhiteSpace method that assembles array list of vmCommands
            removeWhiteSpace(inputFileName);
            if (vmCommands.size()==0) {
                throw new Exception("No commands were written to file.");
            }
        }
    }

    /** Remove white space in input file line by line, and adds output to ArrayList
     *  Preserves spaces within a line
     */
    public void removeWhiteSpace(String inputFileName) throws FileNotFoundException {
        try {
            FileReader fr = new FileReader(inputFileName);
            BufferedReader br = new BufferedReader(fr);

            String line;

            while((line = br.readLine()) != null) {

                String strippedLine = line;

                // Remove comments
                if (!strippedLine.equals("") && strippedLine.contains("//")) {
                    strippedLine = strippedLine.substring(0, strippedLine.indexOf("//")).trim();
                }

                // Add non-empty, condensed commands to array list
                if(!strippedLine.equals("")) {
                    vmCommands.add(strippedLine);
                }
            }

            fr.close();
            br.close();

        }
        catch(FileNotFoundException e)
        {
            throw new FileNotFoundException();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    // Determine the type of command and call appropriate CodeWriter method to translate
    public String parse (String currCommand, String rootName) {

        // Instantiate the CodeWriter
        CodeWriter codeWriter = new CodeWriter();

        // Create vmName to be used for codewriting method
        String vmName = rootName.substring(rootName.lastIndexOf("/")+1);
        String asmCommand = "";

        // Call CodeWriter's writePush method for push writing
        if (currCommand.startsWith("push"))
        {
            asmCommand = codeWriter.writePush(arg1(currCommand), Integer.parseInt(arg2(currCommand)), vmName);
        }

        // Call CodeWriter's writePop method for pop writing
        else if (currCommand.startsWith("pop"))
        {
            asmCommand = codeWriter.writePop(arg1(currCommand), Integer.parseInt(arg2(currCommand)), vmName);
        }

        // Else call writeArithmetic for arithmetic or logical command writing
        else
        {
            asmCommand = codeWriter.writeArithmetic(currCommand);
        }

        // Return the translated command
        return asmCommand;
    }


    // Return the first argument of the current command. If command is not push or pop, command is assumed to be arithmetic,
    // and so command itself (add, sub, etc. is returned.)
    public String arg1(String currCommand){
        if (currCommand.startsWith("push") || currCommand.startsWith("pop"))
        {
            return currCommand.split(" ")[1];
        }
        else
            return currCommand;
    }

    // Return the second argument of the current command. Used only in push or pop case.
    public String arg2(String currCommand){
        return currCommand.split(" ")[2];
    }


    // Return the ArrayList containing the bare VM commands
    public ArrayList<String> getVmCommands () {
        return vmCommands;
    }

}
Nand2Tetris Project 6: The Assembler

Assembler translates programs written in Hack assembly language into binary code that runs on a Hack hardware platform.

This folder contains 4 classes:

Main
Parser
Code
SymbolTable

Major functions of each class:
-Main drives the program to write out the translation to machine language to a .hack file
-Parser parses the input file
-Code supplies the binary codes of the commands
-SymbolTable contains the symbols and their addresses, and its constructor
takes in the parser invoked by Main to accommodate first and second passes

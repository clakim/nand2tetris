import java.io.*;
import java.util.ArrayList;

public class Parser {

    private ArrayList<String> asmCommands = new ArrayList<String>();
    private int pointer = 0;
    private String currCommand;


    public Parser(String inputFileName) throws Exception {
        {
            // Call removeWhiteSpace method that assembles array list of asmCommands
            removeWhiteSpace(inputFileName);
            if (asmCommands.size()!=0) {
                currCommand = asmCommands.get(pointer);
            }
            else {
                throw new Exception("No commands were written to file.");
            }
        }
    }

    // Removes white space in input file line by line, and adds output to arraylist
    public void removeWhiteSpace(String inputFileName) throws FileNotFoundException {
        try {
            FileReader fr = new FileReader(inputFileName);
            BufferedReader br = new BufferedReader(fr);

            String line;

            while((line = br.readLine()) != null) {

                String strippedLine = line;

                // Skip empty lines, remove spaces line by line
                if (!strippedLine.equals("")) {
                    strippedLine = strippedLine.trim().replaceAll("\\s", "");
                }

                // Remove comments
                if (!strippedLine.equals("") && strippedLine.contains("//")) {
                    strippedLine = strippedLine.substring(0, strippedLine.indexOf("//")).trim();
                }

                // Add non-empty, condensed commands to array list
                if(!strippedLine.equals("")) {
                    asmCommands.add(strippedLine);
                }
            }
            //System.out.println(asmCommands);  //tested fine

            fr.close();
            br.close();

        }
        catch(FileNotFoundException e)
        {
            throw new FileNotFoundException();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Method that produces boolean that determines advance
    public boolean hasMoreCommands() {
        boolean hasMore = false;
        if (pointer < asmCommands.size() - 1) {
            hasMore = true;
        }
        return hasMore;

    }

    // Increments pointer to update current command, only called if hasMoreCommands returns true
    public void advance() {
        if (hasMoreCommands()){
            pointer++;
            currCommand = asmCommands.get(pointer);
        }
    }


    // Resets to line 1
    public void restart() {
        pointer = 0;
        currCommand = asmCommands.get(pointer);
    }


    // Enum of command types
    public enum commandType {
        A_COMMAND, C_COMMAND, L_COMMAND
    }

    // Returns command Type based on identifying starting characters
    public commandType commandType() {
        if(currCommand.startsWith("@")) {
            return commandType.A_COMMAND;
        }
        else if(currCommand.startsWith("(")){
            return commandType.L_COMMAND;
        }
        else return commandType.C_COMMAND;
    }

    // Returns symbol or decimal of current command
    public String symbol() {
        if(commandType() == commandType.A_COMMAND) {
            return currCommand.substring(1);
        }
        else if(commandType() == commandType.L_COMMAND) {
            return currCommand.substring(1, currCommand.length()-1);
        }

        else return "";
    }

    // Returns the dest mnemonic in current C-command
    public String dest() {

        if (currCommand.contains("=")){
            return currCommand.substring(0, currCommand.indexOf("="));
        }
        else return "null";
    }


    // Returns comp in current C-command
    public String comp() {

        // Get characters after =
        if (currCommand.contains("="))
        {
            System.out.println("This is comp currCommand with '=' : " + currCommand);
            return currCommand.substring(currCommand.indexOf("=")+ 1);
        }

        // Get characters before ;
        else if (currCommand.contains(";")) {
            return currCommand.substring(0, currCommand.indexOf(";"));
        }
        else return "";

    }


    // Returns jump in current C-command
    public String jump() {
        if (currCommand.contains(";"))
        {
            return currCommand.substring(currCommand.indexOf(";")+1);
        }
        else return "null"; //can't be blank quotes
    }

}
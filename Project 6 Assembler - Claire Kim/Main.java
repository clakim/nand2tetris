import java.io.FileWriter;

public class Main {

    public static void main(String[] args) {

        // Parses command line for filename
        if (args.length == 0) {
            System.out.println("Please specify a filename.");
            System.exit(1);
        }

        if (args.length > 0) {
            try {

                // File I/O operations
                String asmFileName = args[0];
                String hackFileName = asmFileName.replace(".asm", ".hack");
                FileWriter fw = new FileWriter(hackFileName);

                // Instantiate the parser
                Parser parser = new Parser(asmFileName);

                //Instantiate the symbol table, which will undergo the first pass table population
                SymbolTable symbolTable = new SymbolTable(parser);

                // Pass through if L-command
                for (boolean keepGoing = true; keepGoing; ) {
                    if (parser.commandType() == Parser.commandType.L_COMMAND) {
                        if (parser.hasMoreCommands()) {
                            parser.advance();
                            continue;
                        } else {
                            break;
                        }
                    }

                    // Handle A-commands
                    else if (parser.commandType() == Parser.commandType.A_COMMAND) {
                        String binStringHeader = "0";

                        // First, check if number
                        try {
                            // Get integer and convert to string
                            int binCommand = Integer.parseInt(parser.symbol());
                            String binary = Integer.toBinaryString(binCommand);

                            // Pad with as many zeroes necessary given the length of binary string
                            if (binary.length() < 16) {
                                for (int i = 1; i < 16 - binary.length(); i++) {
                                    binStringHeader += "0";
                                }
                            }
                            // Append to the 0 header
                            binStringHeader += binary;
                        }

                        // Catches if not a number
                        catch (NumberFormatException e) {
                            // Second, check if symbol in symbol table
                            if (symbolTable.contains(parser.symbol())) {
                                int binCommand = symbolTable.getAddress(parser.symbol());
                                String binary = Integer.toBinaryString(binCommand);

                                // Pad with zeroes
                                if (binary.length() < 16) {
                                    for (int i = 1; i < 16 - binary.length(); i++) {
                                        binStringHeader += "0";
                                    }
                                }

                                // Append to the 0 header
                                binStringHeader += binary;
                            }


                            // Third, add if not in symbol table
                            else {
                                //Add to symbol table at next spot
                                symbolTable.addEntry(parser.symbol());
                                int binCommand = symbolTable.getAddress(parser.symbol());

                                String binary = Integer.toBinaryString(binCommand);

                                // Pad with zeroes
                                if (binary.length() < 16) {
                                    for (int i = 1; i < 16 - binary.length(); i++) {
                                        binStringHeader += "0";
                                    }
                                }
                                binStringHeader += binary;
                            }
                        }

                        // Write to hack file
                        fw.write(binStringHeader);
                        fw.write("\n");
                    }


                    // Handle C-commands
                    else if (parser.commandType() == Parser.commandType.C_COMMAND) {

                        // Obtain the codes from Code
                        String comp = Code.comp(parser.comp());
                        String dest = Code.dest(parser.dest());
                        String jump = Code.jump(parser.jump());

                        // Set C-command header
                        String binStringHeader = "111";

                        // Append codes to header
                        String fullCcommand = binStringHeader + comp + dest + jump;

                        // Write to hack file
                        fw.write(fullCcommand);
                        fw.write("\n");

                        // Check for un-operable codes
                        if ((dest.equals("N/A") || comp.equals("N/A") || jump.equals("N/A"))) {
                            System.out.println("Hack output corrupt: invalid comp, dest, jump.");
                            fw.close();
                            return;
                        }
                    }

                    // Close and return if standard procedure above does not work
                    else {
                        fw.close();
                        return;
                    }

                    //
                    if (parser.hasMoreCommands()) {
                        parser.advance();
                    } else {
                        break;
                    }
                }
                fw.close();

            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }

        }
    }
}


import java.util.HashMap;

public class SymbolTable {

    private HashMap<String, Integer> symbolTable = new HashMap<String, Integer>();
    private int nextAddress = 16;

    private Parser parser;
    private int pointer = 0;

    // Constructor creates new empty symbol table, uses Parser from Main
    public SymbolTable(Parser parser) {

        symbolTable.put("SP",0);
        symbolTable.put("LCL", 1);
        symbolTable.put("ARG", 2);
        symbolTable.put("THIS", 3);
        symbolTable.put("THAT", 4);

        for (int r=0; r<16; r++)
        {
            symbolTable.put("R" + r, r);
        }

        symbolTable.put("SCREEN", 16384);
        symbolTable.put("KBD", 24576);

        this.parser = parser;
        // Invokes method that passes through for L-commands
        this.firstPass();
    }


    // Works on L-commands to enter all labels with ROM addresses into symbol table
    private void firstPass() {
        for (boolean passFirst = true; passFirst; ) {

            // If L-command, add new entry to symbol table associating with ROM address
            if(parser.commandType()== Parser.commandType.L_COMMAND) {
                symbolTable.put(parser.symbol(), pointer);
            }
            // Else pointer increments if A- or C-command
            else pointer++;

            // Advances if there are more commands
            if(parser.hasMoreCommands()) {
                parser.advance();
            }
            else {
                break;
            }
        }

        // Invokes parser to restart pointer and command from the beginning
        parser.restart();
    }

    // Adds symbol to the table and pairs with next available address
    public void addEntry(String symbol) {
        symbolTable.put(symbol, nextAddress);
        nextAddress++;
    }

    // Boolean: does the symbol table contain the given symbol?
    public boolean contains(String symbol) {
        return symbolTable.containsKey(symbol);
    }

    // Returns the address associated with the symbol
    public int getAddress(String symbol) {
        return symbolTable.get(symbol);
    }

}